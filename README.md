# Important

Generators with _orchestrator work with the orchestrator framework, so use it there.

Credits:

**LAYGO2+SKY130**

1. (Nifty 7) Current Mirror (Kevin)
2. (Nifty 1) Inverter (Lawrence)
3. (Nifty 8) Differential Pair (Ryan)
4. (Nifty 14) Transmission Gate (Lawrence)
5. **NOT NIFTY** 2to1MUX (Lawrence)
6. Seonghyun's 19/50 Nifty Generators (Seonghyun)
   * (Nifty 1) Inverter
   * (Nifty 3) Tie-Zero
   * (Nifty 5) NAND
   * (Nifty 6) NOR
   * (Nifty 7) Current Mirror
   * (Nifty 8) Differential Pair
   * (Nifty 9) Source Follower
   * (Nifty 10) CS Amp
   * (Nifty 11) CS Cascode Amp
   * (Nifty 12) Cascode CG Amp
   * (Nifty 13) CG Amp
   * (Nifty 14) TGate
   * (Nifty 15) 2to1MUX
   * (Nifty 17) CS Amp with Diode Load
   * (Nifty 18) Folded CS Amp with Diode Load
   * (Nifty 19) ClassB PushPull Follower
   * (Nifty 20) Degenerated CS Amp
   * (Nifty 21) Variable Degeneration CS Amp
   * (Nifty 24) PN Bias Voltage Generator

**AnalogBase+cds_ff_mpt (finfet tech)**

1. (Nifty 1) Inverter, NAND Gate (Taeho)
2. NOR Gate (Seonghyun)
3. Tri-state Inverter, NAND Gate (Youngmin)
4. (Nifty 24, 25) PN, Stable Voltage References (Lawrence)
5. NAND Gate, (Nifty 7, 1) Diff Pair, Inverters (Ryan)
6. (Nifty 7) Sophisticated Current Mirrors (Kevin)
